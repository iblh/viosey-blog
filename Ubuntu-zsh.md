---
title: Ubuntu 安装使用 zsh & oh-my-zsh
date: 2016-03-11 14:32
tags:
- Ubuntu
- zsh
- oh-my-zsh
category: 
- 技术向
thumbnail: https://qiniu.viosey.com/imgzsht.jpg
id: 17
---
一直被安利 zsh 的各种好, 前几天在电脑试着用了一下感觉还是不错的,用了 oh-my-zsh 也确实炫酷多了, 所以今天也开始在服务器上折腾 zsh

<!--more-->

## 安装 zsh

```bash
echo $SHELL //查看当且shell
cat /etc/shells //查看当且系统安装的shell
```

```bash
sudo apt-get install zsh
chsh -s /bin/zsh
```

## 安装 oh-my-zsh
[Github: oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
自动安装

```bash
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | sh
```

手动安装

```bash
git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
```

```bash
vim ~/.zshrc
ZSH_THEME="agnoster"
```

使用agnoster主题，需要安装已Patch的字体
[Powerline字体](https://github.com/powerline/fonts) 个人比较喜欢的字体是RobotoMono.

---
>zsh使用方法待后续更新