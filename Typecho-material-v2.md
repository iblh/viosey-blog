---
title: 「从 Alpha 到 2.0，从 1 到 100」 —— 关于 Typecho-Theme-Material 2.0 版本
date: 2016-08-31 04:14
tags:
- typecho
- Theme
- Material Design
category: 
- 创作集
thumbnail: https://cdn.viosey.com/img/blog/snow-slant.png!blogthumbnail
id: 29
---
大概是高中的时候吧，偶然间看到了 [Material Design Lite Blog](https://getmdl。io/templates/blog/index.html) 的这个页面，确实被惊艳到了，一直心心念念想用这个样式写个博客，怎奈那时技术不够 只能就此作罢。

直到今年年初，发现了 typecho，本着轻量的特点决定用它写个博客。随便翻了翻开发文档，感觉开发个 typecho 主题挺简单的嘛，又突然想起了以前那个 material design 的博客静态页面。于是到了寒假就花了大量的时间开始着手这个主题的开发。
刚开始做这个主题的时候，版本号还不敢定为 "1.0"，担心着做不到一半 主题就夭折了，毕竟只有一个静态页面作为基础。自然的 版本号就定为 "Alpha" 了。

从 "Alpha" 到 "Beta"  再到"1.0"，都感到非常excited 觉得离这个主题可以正常使用的程度又近了一大步。
14个 beta 版本，雏形已成。便正式发布了 1.0 版本
回想旧时，完全没料到遗落的愿望可以再次实现。
这时 Github 的 star 数也开始逐渐缓慢的增加，从一直以来的 1 个 star 数，到 10 个，20 个，50 个... 对我而言都像里程碑一样。

从 "1.0" 到 "2.0" 过了有近半年了吧，一直在跳票 \_(:3」∠)_
版本的更新包括了 层出不穷的 bug 的修复，样式的不断改进，响应式的优化，pjax 的使用，多说评论的融合，各种优化，各种自定义项，总之就是很多杂七杂八的各种功能，主题从单调变得愈发的丰富。就好像看着自己的孩子逐渐成长，Github star 数现在也涨到了 100，刚开始做主题时还在想着这个主题能否到达 100 个 star  数，如果可以又不知道需要多久...

可有可无的话讲了这么多，确实得来些正式的 2.0 介绍了

---

## Typecho-Theme-Material 2.0

[关于 Material](https://blog.viosey.com/index.php/Material.html)

[**Github 地址**: typecho-theme-material](https://github.com/viosey/typecho-theme-material) (来 star 一发呗 Nya~)

使用前务必务必务必看 [使用方法](https://github.com/viosey/typecho-theme-material#使用方法)！ 不看使用方法的话有可能会导致一些奇奇怪怪的 bug~

### 大概写下相对于 1.0 版本更新了些什么玩意

- 背景 / 缩略图 的多种样式 (可客制化)
- 简体中文 / 繁体中文 的支持
- 响应式设计 (这个主题在移动端看起来仍然不大友好，屏幕越大效果越好 XD)
- Loading 缓冲效果
- 优化兼容问题 (在 Safari 和 Firefox for Win 上还是有点问题)
- 优化网站访问速度: 删除各种冗余文件，压缩 CSS/js 文件，压缩图片，合并静态资源以减少 HTTP 请求数，增加自定义 CDN 功能
- 融合多说评论 (以及多说样式 MD 化)
- 更新 Markdown 样式
- 美化 "设置外观" 样式 (默认样式太乱了，辣眼睛)
- 添加了 2 个 "时间轴归档" 独立页面和 1 个 "友情链接" 独立页面。
- 增加 pjax 无刷新跳转 功能
- 大概就是这些了吧，要是有漏了些什么就算了吧。反正只需要知道比 1.0 版本好多了就行了 :-D

### 预览 (刚好试了下 "满屏雷姆" 的效果 2333333)

![](https://qiniu.viosey.com/img/TM-2.0-Rem-1.png)