---
title: 网易云音乐的无损与 MP3、WAV 的对比
date: 2016-09-22 00:42
tags:
- Music
- lossless
- Netease
category:
- 分享境 
thumbnail: https://cdn.viosey.com/img/blog/netease_music.png!blogthumbnail
id: 32
---
今晚买了一个月的网易云会员, 就随手测试了一下无损质量

<!-- more -->

>**以下图片可在新标签中打开查看大图**

---

# Eagles - Hotel California (加州旅馆)

### 320k (mp3)
![](https://cdn.viosey.com/img/blog/HC-1mp3.png)

### 网易无损 (Flac)
![](https://cdn.viosey.com/img/blog/HC-2flac.png)

### 母带级无损 (WAV)
![](https://cdn.viosey.com/img/blog/HC-3wav.png)

# 蔡琴 - 渡口

### 320k (mp3)
![](https://cdn.viosey.com/img/blog/DK-1mp3.png)

### 网易无损 (Flac)
![](https://cdn.viosey.com/img/blog/DK-2flac.png)

### 网上流传甚广的 600Mb 母带级无损 (WAV)

>很显然  是**假无损**


![](https://cdn.viosey.com/img/blog/fDK-3wav.png)


## 写在最后
除了这两首, 还对比了一下其他的 就不一一放上截图了

讲道理 网易无损和 320k 差别并不是很大, 并没有什么必要购买会员, 想要好的无损还是在各种论坛/贴吧慢慢找吧