---
title: VOEZ 兰空
date: 2016-06-02 21:45
tags:
- Music
- Game
category: 
- 分享境
thumbnail: https://cdn.viosey.com/img/blog/VOEZ.jpg!blogthumbnail
id: 24
toc: false
---
音游 「Cytus」「Deemo」的公司 Rayark 昨天在 Android 平台发行了新的一款音游 「VOEZ 兰空」

<blockquote class="twitter-tweet" data-lang="zh-cn"><p lang="en" dir="ltr">VOEZ on Google Play now!<a href="https://t.co/43yqE0RHFD">https://t.co/43yqE0RHFD</a></p>&mdash; VOEZ (@VoezRayark) <a href="https://twitter.com/VoezRayark/status/738024664287449089">2016年6月1日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

游戏本身免费，但是有内购。LOGO 和 宣传图简直美炸了 (๑•̀ㅂ•́)و✧
于是就高高兴兴的下载了，结果打开的时候闪退了！(#\`O′) 居然还不支持 Android N！只能用 iPad 体验了...

游戏内任何一个画面都感觉好美好美，线条和图片构成极简的画面
![](https://cdn.viosey.com/img/blog/VOEZ-1.jpg!osg)

达到成就之后还会有日记解锁，这点和「Deemo」挺像的，日记的剧情和图片都是日系清新风格
![](https://cdn.viosey.com/img/blog/VOEZ-2.jpg!osg)


除了清新的画面和空灵的音乐旋律，在谱面设计方面，「VOEZ」还是做的不错的，音符变成了菱形，按照音乐的节奏，逐步变幻出可单击、长按、滑动的音符，节奏的贴合效果也做的很棒，特别是 Perfect/Ok 会有不同的光芒，UX  MAX！

![](https://cdn.viosey.com/img/blog/VOEZ-3.jpg!osg)