---
title: 快速创建与发布 npm 包
date: 2016-10-23 14:43:20
tags:
- node.js
- npm
category:
- 技术向
thumbnail: https://cdn.viosey.com/img/blog/npm-redbg.jpg!blogthumbnail
id: 35
---
hexo-theme-material 也发布了一个 npm 包，除了使用 git，也可以使用 npm 啦。

<!-- more -->

地址：[hexo-material](https://www.npmjs.com/package/hexo-material)

## 创建

>注意创建模块前，先去 npm 官网确认模块名是否未被占用

```bash
npm init
```

添加账号：
```
$ npm adduser   
Username: your name
Password: your password
Email: yourmail@gmail.com
```

## 发布

```bash
npm publish
```

编辑 `.npmignore`
>如果文件夹中存在 `.gitignore`， 则 `.npmignore` 与之相同；
如果想忽略一些 `.gitignore` 中没有包括的东西，那么创建一个空的 `.npmignore` 可覆盖之.

npm 社区版本号规则采用的是 [semver](http://semver.org/) (语义化版本)

## 报错

发布时报错：`no_perms Private mode enable, only admin can publish this module`

因为 npm 镜像源会有延时，替换为官方源即可。

```bash
npm config set registry http://registry.npmjs.org
```

然后重新 `adduser`


## 镜像源

1. [临时] 通过 config 配置指向国内镜像源

```bash
# 配置指向源npm info express
npm config set registry http://registry.cnpmjs.org
```

2. [临时] 通过 npm 命令指定下载源

```bash
npm --registry http://registry.cnpmjs.org info express
```

3. 在配置文件 ~/.npmrc 文件写入源地址

```bash
//打开配置文件
vim ~/.npmrc
//写入配置文件
registry =https://registry.npm.taobao.org
```

[淘宝 npm 镜像](https://npm.taobao.org/)

