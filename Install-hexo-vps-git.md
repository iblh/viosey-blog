---
title: 在 VPS 上搭建 Hexo 博客，使用 Git 部署
date: 2016-10-05 21:09:57
tags:
- hexo
- Git
- Ubuntu
- VPS
category: 
- 技术向
thumbnail: https://cdn.viosey.com/img/blog/hexo-logo.png!blogthumbnail
id: 33
---
## 写在前面

因为 hexo 的搭建和使用与其他博客程序不同，而且搜索到的很多结果都是在 Github 上搭建 hexo 的教程。
<!-- more -->
于是就有很多朋友问我 VPS 也可以搭建 hexo 吗？
刚好前几天在迁移博客的时候也查了一些资料，现在再整理一遍。

###  方案

Hexo 本地生成静态文件，再部署到 VPS 上，用 Nginx 做 Web 服务器。

###  准备
- VPS
- Macintosh / Hackintosh OS

## 本地环境

### Node.js

```bash
$ brew install node
```

### 创建 Hexo 目录

```bash
$ mkdir "your hexo dir name"
$ cd "your hexo dir name"
```

### 安装 Hexo

```bash
$ npm install -g hexo-cli
```

如果上面这行安装命令报错了，可以试试看下面这行

```
$ sudo npm install -g hexo-cli --unsafe
```

安装完成之后

```bash
$ hexo init
$ npm install
$ hexo d -fg
$ hexo serve
```

打开 http://localhost:4000 如果看到 hexo 的初始页面证明安装成功。

### 生成 SSH 公钥密钥

```bash
$ cd ~/.ssh
$ ssh-keygen
```

它先要求你确认保存公钥的位置（.ssh/id_rsa），然后它会让你重复一个密码两次，如果不想在使用公钥的时候输入密码，可以留空。

[GitHub Help - Generating a new SSH key and adding it to the ssh-agent](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)

## 服务器环境 (以 Ubuntu 为例)

### 安装 Git & Node.js

```bash
$ apt-get install git
$ curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
$ apt-get install -y nodejs
```

### 创建 git 用户 & 禁用 git 用户的 shell 登录权限

```bash
$ adduser git
$ vim /etc/passwd
```

将

```vim
git:x:1001:1001:,,,:/home/git:/bin/bash
```

改为

```vim
git:x:1001:1001:,,,:/home/git:/usr/bin/git-shell
```

### 添加证书登录

前面已经生成了公钥，现在把 `.pub` 公钥里的内容添加到服务器的 `/home/git/.ssh/authorized_keys` 文件中

### 初始化 Git 仓库

目录可自己选择

```bash
$ mkdir "your git dir name"
$ cd "your git dir name"
$ git init --bare hexo.git
```

使用 `--bare` 参数，Git 就会创建一个裸仓库，裸仓库没有工作区，我们不会在裸仓库上进行操作，它只为共享而存在。

### 配置 git hooks

在 `hexo.git/hooks` 目录下新建一个 `post-receive` 文件：

```bash
$ cd /var/repo/hexo.git/hooks
$ vim post-receive
```

在  `post-receive` 文件中写入如下内容：

```vim
#!/bin/sh
git --work-tree=/www/hexo --git-dir=/var/hexo.git checkout -f
```

>其中 `/www/hexo` 为部署目录，`/var/hexo.git` 为该 git 仓库。

设置这个文件的可执行权限：

```bash
$ chmod +x post-receive
```

[Git 钩子](https://git-scm.com/book/zh/v2/%E8%87%AA%E5%AE%9A%E4%B9%89-Git-Git-%E9%92%A9%E5%AD%90)

### 设置拥有者

```bash
$ chown -R git:git hexo.git
$ chown -R git:git /www/hexo
```

## 本地配置

现在配置 hexo 的 deploy。

修改 hexo 目录下的 `_config.yml` 找到 `deploy`, 修改为：
> `repo` 的地址为你自己的地址以及 git 仓库目录

```yaml
deploy:
    type: git
    repo: git@www.example.com:/var/hexo.git
    branch: master
```

## 开始使用

新建文章：

```bash
$ hexo new "post name"
```

生成 & 部署：

```bash
$ hexo clean && hexo g && hexo d
```

## 参考
- [服务器上的 Git - 生成 SSH 公钥](https://git-scm.com/book/zh/v1/%E6%9C%8D%E5%8A%A1%E5%99%A8%E4%B8%8A%E7%9A%84-Git-%E7%94%9F%E6%88%90-SSH-%E5%85%AC%E9%92%A5)
- [自定义 Git - Git 钩子](https://git-scm.com/book/zh/v2/%E8%87%AA%E5%AE%9A%E4%B9%89-Git-Git-%E9%92%A9%E5%AD%90)
- [Generating a new SSH key and adding it to the ssh-agent](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)
- [搭建 Git 服务器](http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000/00137583770360579bc4b458f044ce7afed3df579123eca000)
- [使用 git hooks 进行 hexo 博客自动化部署](http://sumyblog.me/2015/11/02/use-git-hooks-for-hexo-automatic-deployment/)
- [VPS(CentOS) 搭建 Hexo 博客与 Git Hooks 更新](http://www.hansoncoder.com/2016/03/02/VPS-building-Hexo/)