---
title: 重装+升级了一次服务器, 已启用 http2
date: 2016-08-30 00:37
tags:
- Ubuntu
- Nginx
- http2
- PHP7
category: 
- 技术向
thumbnail: https://cdn.viosey.com/img/blog/nginx-http2.png!blogthumbnail
id: 28
---
因为感觉服务器有点乱, 所以前几天重装了一次 Ubuntu 14.04, 顺便升级了 Nginx1.10 和 PHP7, 然后再开启了一下 http2.
不过遇到了些坑, 也耗了大概一天的时间.

-------

## 安装 PHP7 和 Nginx1.10(Stable version) 首先需要添加的 PPA

```shell
sudo apt-get install python-software-properties software-properties-common
sudo add-apt-repository ppa:ondrej/nginx
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
```

## 安装 PHP7 和 Nginx1.10(Stable version)

```shell
sudo apt-get install nginx -y
sudo apt-get install php7.0-fpm php7.0-mysql php7.0-common php7.0-curl php7.0-cli php7.0-mcrypt php7.0-mbstring php7.0-dom
```

## 开启 http2

直接在 nginx 配置文件中的 listen 后面增加 `http2` 就行了, 不过 SPDY 和 http2 是不能同时使用的, 否则会报错.

---
### 遇到的一些坑

- 第一次开启 http2 的时候, nginx 配置已经搞定, 但是 protocol 仍然是 http/1.1

	Google 了一下 得知 chrome 51 版本之后需要 openSSL 1.0.2+ 的版本于是又把 openSSL 升级到了 1.0.2
发现还是不行!

	后来问了下基友才知道 nginx 是使用内置的 openSSL _(:3」∠)_

- 重装得差不多的时候, 发现 php 页面不显示了! ( Excuse Me) 
	php 页面都显示为空白页面, 这下又查了好久, 在 stackoverflow 找到了解决方案:
	把 nginx 配置里面的 `include fastcgi_params` 替换为 `include fastcgi.conf`
	
	话说回来 总不能就这样莫名其妙的解决了但是不知道原因吧, 找到的解释是这样的:
	nginx 的 这两份 fastcgi 并没有太大的差异，唯一的区别是后者比前者多了一行「SCRIPT_FILENAME」的定义：`fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;` 然后 balabalabala...
	
- 最后一个坑其实之前也遇见过, 就是装完 typecho 后, 后台管理进不去 (似乎是 404? ) 我还差点以为是 typecho 不兼容 PHP7
	Anyway, 方法还是修改 nginx 配置文件 改成酱紫:
    ```
    location ~ [^/]\.php(/|$) { 
    	*****
    	*****
    	*****
    }
    ```