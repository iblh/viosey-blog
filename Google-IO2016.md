---
title: 2 小时的未来
date: 2016-05-19 21:30
tags: 
- Google
- Android
category: 
- 分享境
thumbnail: https://cdn.viosey.com/img/blog/Google-io2016.jpg!blogthumbnail
id: 23
toc: false
---
>苹果发布的是产品, 谷歌发布的是未来

<!--more-->


虽然没有撑到凌晨 3点，不过 Google I/O 也至少看了一半。开场 LiveHouse 音乐挺不错的，然而并没有找到是什么歌曲。

基于 Google Now 的 **Google Assistant** 也是 excited 了一把，但在中文环境下估计功能性又要被砍去一大半。

![](https://cdn.viosey.com/img/blog/GoogleAssistant.gif)

又基于 Google Assistant 的 **Google Home** 的演示真实的带来了未来居家生活的感觉，只需一两句话就能处理好繁琐的日常事务。

![](https://cdn.viosey.com/img/blog/GoogleHome.jpg)

当知道 Android N 仍未取想好的时候, 懵逼的应该不止我一个人吧...Google 因此专门搞了个网站， 一起来[给 Android N 取个名字](http://android.com/n)。我滚键盘的时候, 给它取了「nartjie」这个名字。

一觉醒来之后，Google 也给我推送了 Android N (NPD35K) 的更新，不更白不更嘛。不知道是不是心理作用总觉得比之前流畅多了, 倒是更完之后才发现支付裱又用不了了 >\_< 

---

同样是熬了 1 小时的夜，感觉 Google I/O 比苹果的春季发布会不知道高到哪里去了。

![](https://cdn.viosey.com/img/blog/Google-io2016.png)