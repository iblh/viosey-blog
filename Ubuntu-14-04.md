---
title: Ubuntu 14.04 LTS 配置流程
date: 2016-02-07 16:16
tags:
- VPS
- Ubuntu
category: 
- 技术向
thumbnail: https://qiniu.viosey.com/2016-08-29_ubuntu-14.0.4-1.jpg
id: 13
---
>玩VPS经常会遇到一些奇奇怪怪的bug，实在解决不了干脆就重装系统了。
重装后肯定又得把系统调教一番，于是就有了这篇调教攻略（手动斜眼），毕竟调教也要按照基本法

<!--more-->

---
## 0x00 更改root密码/切换root
```
sudo passwd root
sudo su
```
---
## 0x01 更新
```
apt-get update
apt-get upgrade
```
---
## 0x02 自定义
###  PS1
```
vi ~/.bashrc
```
#### 修改
```
export PS1="\[\e[37m\]>>>\[\e[0m\] \h [\u]: \[\e[37m\]\w \[\e[0m\] $ " 
export PS1="\[\e[37m\]>>>\[\e[0m\] \h [\u]: \[\e[37m\]\w \[\e[0m\] # " //root
```
或
```
if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\[\e[37m\]>>>\[\e[0m\] \h [\u]: \[\e[37m\]\w \[\e[0m\] $ '
```
#### 格式
```
显示符        意义
echo $PS1 ：	可以得到PS1的值
PS1 ：	    用户平时的提示符
PS2 ：	    第一行没输完，等待第二行输入的提示符
\d ：	    代表日期，格式为weekday month date，例如："Mon Aug 1"
\H ：	    完整的主机名称。例如：我的机器名称为：fc4.linux，则这个名称就是fc4.linux
\h ：	    仅取主机的第一个名字，如上例，则为fc4，.linux则被省略
\t ：	    显示时间为24小时格式，如：HH：MM：SS
\T ：	    显示时间为12小时格式
\A ：	    显示时间为24小时格式：HH：MM
\u ：	    当前用户的账号名称
\v ：	    BASH的版本信息
\w ：	    完整的工作目录名称。家目录会以 ~代替
\W ：	    利用basename取得工作目录名称，所以只会列出最后一个目录
\# ：        下达的第几个命令
提示字符 ：	 如果是root时，提示符为：# ，普通用户则为：$
```
#### 设置颜色：
```
\e[F;Bm
\[\e[F;Bm\]
```
```
\[     begin a sequence  of  non-printing  characters,
 which could  be  used  to  embed a terminal control sequence into the prompt.
\]     end a sequence of non-printing characters.
```
>其中F''为字体颜色，编号30~37；B''为背景色，编号40~47。
可通过``\\e[0m''关闭颜色输出；特别的，当B为1时，将显示加亮加粗的文字，详细请看下面的颜色表与代码表。

```
颜色表	    前景	 背景
黑色	     30	    40
红色	     31	    41
绿色	     32	    42
黄色	     33	    43
蓝色	     34	    44
白色	     37	    47
紫红色	     35	    45
青蓝色	     36	    46
```
```
结尾代码	 意义
0	        OFF
1        	高亮显示
4	        underline
5	        闪烁
7	        反白显示
8	        不可见
```

###  vim
```
vi ~/.vimrc
```
```
:set number         显示行号
:set relativenumber 显示相对行号
:set hlsearch       搜索结果高亮
:set autoindent     自动缩进
:set smartindent    智能缩进
:set tabstop=4      设置 tab 制表符所占宽度为 4
:set softtabstop=4  设置按 tab 时缩进的宽度为 4
:set shiftwidth=4   设置自动缩进宽度为 4
:set expandtab      缩进时将 tab 制表符转换为空格
:filetype on        开启文件类型检测
:syntax on          开启语法高亮
```

###  参考文档
>[CustomizingBashPrompt](https://help.ubuntu.com/community/CustomizingBashPrompt)
>[.bashrc PS1 generator](http://bashrcgenerator.com/)

---
## 0x03 创建root权限用户
###  添加用户
```
adduser username
passwd username
```
###  赋予root权限
#### 方法一：
把 "Allows people..." 前面的注释（#）去掉
```
vi /etc/sudoers

# Allows people in group wheel to run all commands
%wheel    ALL=(ALL)    ALL
```
然后修改用户，使其属于root组（wheel），命令如下：
```
usermod -g root username
```
修改完毕，现在可以用username帐号登录，然后用命令 su – ，即可获得root权限进行操作。


#### 方法二：
在root下面添加一行，如下所示：
```
vi /etc/sudoers

# Allow root to run any commands anywhere
root    ALL=(ALL)     ALL
username   ALL=(ALL)     ALL
```
修改完毕，现在可以用username帐号登录，然后用命令 sudo – ，即可获得root权限进行操作。


#### 方法三：
把用户ID修改为 0 ，如下所示：
```
vi /etc/passwd

username:x:0:33:username:/data/webroot:/bin/bash
```

###  参考文档
>[linux下添加用户并赋予root权限](http://blog.csdn.net/hellozpc/article/details/46952595)
 
---
## 0x04 安装配置VSFTPD
```
apt-get install vsftpd
```
```
vsftpd服务        开启      停止	   重启
service vsftpd    start    stop    restart
```
修改vsftpd的配置文件
```
vi /etc/vsftpd.conf
```
```
#禁止匿名访问
anonymous_enable=NO

#接受本地用户
local_enable=YES

#可以上传
write_enable=YES

local_umask=022
```
---
## 0x05 安装 Nginx+MySQL+PHP
### 1. 安装apt源管理工具、添加nginx和php的安装源
```
apt-get install python-software-properties
add-apt-repository ppa:nginx/stable
add-apt-repository ppa:ondrej/php5
```
### 2. 安装mysql
P.S. 在安装过程中，会要求你输入MySQL的root账号的密码
```
apt-get install mysql-server
```
### 3. 安装php及对mysql的支持
```
apt-get install php5 php5-fpm php5-mysql php-apc
```
根据实际需要，选择性的安装php的各类功能模块
```
apt-get install php-pear php5-dev php5-curl
apt-get install php5-gd php5-intl php5-imagick
apt-get install php5-imap php5-mcrypt php5-memcache
apt-get install php5-ming php5-ps php5-pspell
apt-get install php5-recode php5-snmp php5-sqlite
apt-get install php5-tidy php5-xmlrpc php5-xsl
```
### 4. 安装nginx
```
apt-get install nginx
```
### 5. 配置php
```
vi /etc/php5/fpm/php.ini
```
### 6. 配置nginx
创建一个 /www 目录设置为 755 权限
```
mkdir /www
chmod 755 /www
```
更换网站目录：
```
vi /etc/nginx/sites-enabled/default
```
把 root /usr/share/nginx/html  改为：root **/www**
```
找到：root /usr/share/nginx/html;
改为：root /www;
```
在默认索引文件中增加 index.php
```
找到：index index.html index.htm;
改为：index index.php index.html index.htm;
```
找到：**location ~ .php$ { * }** 区块
做如下调整（改动之处于行末有注释）：
```
location ~ .php$ {
try_files $uri =404; #增加
fastcgi_split_path_info ^(.+.php)(/.+)$; #反注释
## NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini
#
## With php5-cgi alone:
# fastcgi_pass 127.0.0.1:9000;
## With php5-fpm:
fastcgi_pass unix:/var/run/php5-fpm.sock; #反注释
fastcgi_index index.php; #反注释
include fastcgi_params; #反注释
}
```

### 7. 解决php-fpm与nginx的小bug
由于nginx与php-fpm之间的一个小bug，会导致这样的现象：
网站中的静态页面 *.html 都能正常访问，而 *.php 文件虽然会返回200状态码，
但实际输出给浏览器的页面内容却是空白。
简而言之，原因是nginx无法正确的将 *.php 文件的地址传递给php-fpm去解析，
相当于php-fpm接受到了请求，但这请求却指向一个不存在的文件，于是返回空结果
```
vi /etc/nginx/fastcgi_params
```
在文件的最后增加一行：
```
fastcgi_param   SCRIPT_FILENAME     $document_root$fastcgi_script_name;
```
>关于这行的内容，多说几句，其中有两个参数：
$document_root 即是指网站的根目录，也就是我们在前面刚设置的 root /www;
$fastcgi_script_name 则是指网站上的各个 *.php 文件名（其实是文件的相对路径）
这两个合在一起形成完整的 php file path，比如你的网站有个 /test/script.php 文件，
nginx传递给php-fpm的完整路径就是：/www/test/script.php

### 8. 重启各项服务

重新加载各项配置改动
```
service php5-fpm reload
service nginx reload
```
查看Nginx错误情况
```
sudo nginx -t
nginx -c /etc/nginx/nginx.conf -t
```
### 9. 测试
```
vi /www/index.php
```
```
<?php echo phpinfo(); ?>
```
访问：http://localhost 若一切正常，将输出php环境信息
### 10. 安装phpmyadmin
```
apt-get install phpmyadmin
```
```
ln -s /usr/share/phpmyadmin /www/phpmyadmin
```
访问：http://localhost/phpmyadmin

###  参考文档
>[Ubuntu 14.04安装nginx+php+mysql](http://www.cnblogs.com/helinfeng/p/4219051.html)
>[Ubuntu安装 nginx, MySQL, PHP, phpmyadmin, WordPress](http://www.cnblogs.com/eecs/p/3963869.html)

---
 ![](https://viosey.com/blog/usr/uploads/2016/02/1897819281.jpg)