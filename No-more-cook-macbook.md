---
title: 不想也不会再买 MacBook 了
date: 2016-10-29 10:46:56
tags:
- MacBook
category: 自言语
thumbnail: https://cdn.viosey.com/img/blog/macbookpro-2016_large.png!blogthumbnail
id: 36
toc: false
---
昨天（中国时区）的苹果发布会没看，也没打算看。
<!-- more -->
Touch Bar 和其他数据的提前泄露已经足够了。再说，以苹果现在的尿性，开头应该少不了一段吹嘘。

也确信不会有什么黑科技。

>至于为何不再买 MacBook，我想强调这里指的是 **库克时期** 的 MacBook。
>
>**声明：**以下观点仅表示个人看法和吐槽。

一直都不觉得自己是个果粉，从未买过一只 iPhone。而对于电脑，我喜欢的只是 `macOS`。

和一开始就混家酿俱乐部的乔布斯不同的是，库克多半不是电脑 geek。

我觉得 MacBook 开始迎合 iPhone，而目的不外乎就是想利用庞大的 iPhone 用户基数。

于是苹果的消费产品和生产工具的界线就模糊了。

举几个栗子：

- Touch Bar 看起来不过是新颖了一些，实用性呢？宁愿用 ESC 换掉 Touch ID。
- 接口的槽大家也都吐过了。
- 噢对了，还有自行升级的可能性。

用做手机的思路去做电脑吃枣药丸。消费产品随便你改，生产工具把很好的功能直接去掉绝不是明智之举。

虽然觉得离不开 macOS，但在库克时期，也不再考虑 MacBook，而是垃圾桶 / Mac mini。对一成不变的 MacBook 外观也审美疲劳了。