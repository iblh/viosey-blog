---
title: Let's Encrypt SSL 证书配置
date: 2016-02-09 23:26
tags:
- SSL
- HTTPS
category: 
- 技术向
thumbnail: https://qiniu.viosey.com/imgSSLLock.jpg
id: 14
---
正准备发布这篇SSL配置文章的时候遇到了很巧合的事——通过Google首页得知今天是2016年的Safer Internet Day (02/09/2016)

Safer Internet Day相关简介
>SAFER INTERNET DAY
Safer Internet Day (SID) is organised by Insafe in February of each year to promote safer and more responsible use of online technology and mobile phones, especially among children and young people across the world.


<!--more-->

----------


至于为什么要给自己的网站配上SSL，因为强迫症把**chrome://flags/**中的
"Mark non-secure origins as non-secure"打开之后，http网站一律显示为红叉锁头（如下图）
![](https://qiniu.viosey.com/imghttp.png)

因此看自己的网站确实那么一丝不爽。(´◔ω◔)
经过几天的尝试和配置后，
看着绿色的锁头和全绿的评级，简直不要太爽。o(*≧▽≦)ツ


![](https://qiniu.viosey.com/imgSSL-Qualys.png)


## 0x00 获取Let's Encrypt
###  安装git

```bash
apt-get install git 
```

###  git Let's Encrypt

```bash
cd /etc
git clone https://github.com/letsencrypt/letsencrypt.git
cd letsencrypt
```

## 0x01 正式开始生成证书
多个域名可以加多个-d 域名，注意替换邮箱、域名和网站目录

```bash
./letsencrypt-auto certonly --email viosey@outlook.com -d viosey.com -d www.viosey.com --webroot -w /www --agree-tos
```

###  部分输出过程

>Updating letsencrypt and virtual environment dependencies...../root/.local/share/letsencrypt/local/lib/python2.7/site-packages/pip/_vendor/requests/packages/urllib3/util/ssl_.py:315: SNIMissingWarning: An HTTPS request has been made, but the SNI (Subject Name Indication) extension to TLS is not available on this platform. This may cause the server to present an incorrect TLS certificate, which can cause validation failures. For more information, see https://urllib3.readthedocs.org/en/latest/security.html#snimissingwarning.
	SNIMissingWarning
/root/.local/share/letsencrypt/local/lib/python2.7/site-packages/pip/_vendor/requests/packages/urllib3/util/ssl_.py:120: InsecurePlatformWarning: A true SSLContext object is not available. This prevents urllib3 from configuring SSL appropriately and may cause certain SSL connections to fail. For more information, see https://urllib3.readthedocs.org/en/latest/security.html#insecureplatformwarning.
	InsecurePlatformWarning
/root/.local/share/letsencrypt/local/lib/python2.7/site-packages/pip/_vendor/requests/packages/urllib3/util/ssl_.py:120: InsecurePlatformWarning: A true SSLContext object is not available. This prevents urllib3 from configuring SSL appropriately and may cause certain SSL connections to fail. For more information, see https://urllib3.readthedocs.org/en/latest/security.html#insecureplatformwarning.
	InsecurePlatformWarning
.
Requesting root privileges to run with virtualenv: /root/.local/share/letsencrypt/bin/letsencrypt certonly --email viosey@outlook.com -d viosey.com -d www.viosey.com --webroot -w /www --agree-tos

###  证书生成成功

```bash
IMPORTANT NOTES:
 - If you lose your account credentials, you can recover through
	 e-mails sent to viosey@outlook.com.
 - Congratulations! Your certificate and chain have been saved at
	 /etc/letsencrypt/live/viosey.com/fullchain.pem. Your cert will
	 expire on 2016-05-02. To obtain a new version of the certificate in
	 the future, simply run Let's Encrypt again.
 - Your account credentials have been saved in your Let's Encrypt
	 configuration directory at /etc/letsencrypt. You should make a
	 secure backup of this folder now. This configuration directory will
	 also contain certificates and private keys obtained by Let's
	 Encrypt so making regular backups of this folder is ideal.
 - If you like Let's Encrypt, please consider supporting our work by:

	 Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
	 Donating to EFF:                    https://eff.org/donate-le

```


## 0x02 Nginx虚拟主机的设置
###  配置Nginx

```bash
vi /etc/nginx/nginx.conf
放在 http { * } 里 
```

或者 

```bash
vi etc/nginx/sites-enabled/default
```

```
server {
	listen 443 ssl;   
	server_name viosey.com;     //这里是你的域名
	index index.html index.htm index.php default.html default.htm default.php;
	root /www;            //网站目录
	ssl_certificate /etc/letsencrypt/live/viosey.com/fullchain.pem;    //前面生成的证书，改一下里面的域名就行，不建议更换路径
	ssl_certificate_key /etc/letsencrypt/live/viosey.com/privkey.pem;  //前面生成的密钥，改一下里面的域名就行，不建议更换路径
	ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_prefer_server_ciphers on;
	ssl_session_cache shared:SSL:10m;
	
	include wordpress.conf;  //这个是伪静态根据自己的需求改成其他或删除
	#error_page 404 /404.html;
	location ~ [^/]\.php(/|$) {
			# comment try_files $uri =404; to enable pathinfo
			try_files $uri =404;
			fastcgi_pass unix:/tmp/php-cgi.sock;
			fastcgi_index index.php;
			include fastcgi.conf;     //lnmp 1.0及之前版本替换为include fcgi.conf;
			#include pathinfo.conf;
	}
	
	location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$ {
			expires 30d;
	}
	
	location ~ .*\.(js|css)?$ {
			expires 12h;
	}
	
	access_log off;
}
```

Example:

```
 server {
	listen 443 ssl;
	server_name viosey.com;
	index index.html index.htm index.php default.html default.htm default.php;
	root /www;
	ssl_certificate /etc/letsencrypt/live/viosey.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/viosey.com/privkey.pem;
	ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_prefer_server_ciphers on;
	ssl_session_cache shared:SSL:10m;

	#error_page 404 /404.html;
	location ~ [^/]\.php(/|$) {
					# comment try_files $uri =404; to enable pathinfo
					try_files $uri =404;
					fastcgi_pass unix:/tmp/php-cgi.sock;
					fastcgi_index index.php;
					#include pathinfo.conf;
	}

	location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$ {
					expires 30d;
	}

	location ~ .*\.(js|css)?$ {
					expires 12h;
	}

	access_log off;
}
```


## 0x03 证书续期
###  安装Crontab

```bash
apt-cache show cron         //大部分情况下Debian都已安装
apt-get install cron
/etc/init.d/cron restart    //重启Crontab
```

- Crontab 使用命令
>1. 查看crontab定时执行任务列表
crontab -l
>2. 添加crontab定时执行任务
crontab -e

- Crontab 任务命令书写格式

```
格式：	minute	  hour	       date	      month	    day                command
解释：	分钟	     小时 	    日期	      月        	周                   命令
范围：	0-59	  0～23	      1～31       1～12	  0～7(0和7都代表周日)
```

```
符号	意义	            例子                    	    表示
*	代表所有有效的值。	0 23 * * * backup	        无论几月几日周几的23点整都执行backup命令
,	代表分割开多个值。	30 9 1,16,20 * * command	每月的1、16、20号9点30分执行command命令
-	代表一段时间范围。	0 9-17 * * * checkmail	    每天9点到17点的整点执行checkmail命令
/n	代表每隔n长时间。	 */5 * * * * check           每隔5分钟执行一次check命令，与 0-59/5 一样
```

```
例子	                                写法
每天凌晨3:00执行备份程序：        	      0 3 * * * /root/backup.sh
每周日8点30分执行日志清理程序：	        30 8 * * 7 /root/clear.sh
每周1周5 0点整执行test程序：	           0 0 * * 1,5 test
每年的5月12日14点执行wenchuan程序：   	   0 14 12 5 * /root/wenchuan
每晚18点到23点每15分钟重启一次php-fpm：	  */15 18-23 * * * /etc/init.d/php-fpm
```

###  创建sh, 更新cron

```bash
cat >/etc/letsencrypt/renew-ssl.sh<<EOF
#!/bin/bash
mkdir -p /网站目录完整路径/.well-known/acme-challenge
/root/letsencrypt/letsencrypt-auto --renew-by-default certonly --email viosey@outlook.com -d viosey.com -d www.viosey.com --webroot -w /www --agree-tos
EOF
chmod +x /etc/letsencrypt/renew-ssl.sh
```

```bash
crontab -e

0 3 */30 * * /etc/letsencrypt/renew-ssl.sh

service cron restart
```

## 0x04 301重定向https

###  配置Nginx：

```bash
vi /etc/nginx/nginx.conf
```

或者 

```bash
vi etc/nginx/sites-enabled/default
```

>如果server同时含有443和80端口，把80监听去掉

```
server {
	listen 80;
	server_name viosey.com ;
	return 301 https://viosey.com$request_uri;
}
```

###  重启nginx

```bash
service nginx reload
```

## 参考文档
>[免费SSL安全证书Let's Encrypt安装使用教程(附Nginx/Apache配置)](http://www.vpser.net/build/letsencrypt-free-ssl.html)
>[Let's Encrypt，免费好用的 HTTPS 证书](https://imququ.com/post/letsencrypt-certificate.html)
>[CentOS Nginx 安装Let’s Encrypt 免费ssl证书](https://fatesinger.com/77407)
>[Nginx HTTPS 详细配置](https://fatesinger.com/75967)
>[How to protect your Debian or Ubuntu Server against the Logjam attack](https://www.howtoforge.com/tutorial/how-to-protect-your-debian-and-ubuntu-server-against-the-logjam-attack/)
>[SSL Labs 评分 A+ 的 nginx 配置](http://songchenwen.com/tech/2015/09/09/nginx-configuration-with-ssl-labs-class-a-plus/)
