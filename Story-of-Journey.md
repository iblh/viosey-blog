---
title: Story of Journey
date: 2017-04-04 00:10:11
tags:
- PS4
- 风之旅人
- Journey
- 多图杀猫
- 第九艺术
category: 分享境
thumbnail: https://cdn.viosey.com/img/blog/journey-game-screenshot.png!blogthumbnail
---
>多图预警

## Chapter 1
两周之前心血来潮，买了台 PS4，其独占的《风之旅人》(Journey) 是长草的游戏之一。
大概在高中的时候就听朋友安利这款游戏，《纪念碑谷》似乎也是以其为原型。

>刚到手的 PS4，一开始为横放，但太占位置了，于是便换成了立式

![PS4](https://qiniu.viosey.com/img/IMG_20170319-PS4.jpg)

## Chapter 2
![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402112041.jpg)

初次进入旅途，一切都是新鲜而又充满未知，跌跌撞撞地四处探索。

刚开始一片沙漠，四处茫茫沙海，在远处依稀有一道光柱，带着疑惑朝之缓步前行。

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170322132000.jpg)

《风之旅人》 场景十分细致，光影交错，原声更是渲染了强烈的代入感，实为第九艺术的典范。

游戏中按「○」会出现一个符号，在遇见其他旅人之后才意识到这是每个人独有的象征。

>一周目的符号，感觉挺好看的

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170322201613.jpg)

《风之旅人》会随机遇到不同的伙伴，但是不能语音，也不能发消息，只能默默的陪伴前行，交流只有按下「○」键发出的声响和符号，直到游戏最后才能看到对方的游戏 ID。

>一周目遇到的同伴

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170322202455.jpg)


## Chapter 3
结束了一周目，对于 Journey 的关卡有了大概的了解后，于是二周目开始计划解锁奖杯。
为了尽早获得白袍，所以参考了这个攻略 [風之旅人 - 全符文壁畫攻略](https://forum.gamer.com.tw/Co.php?bsn=60281&sn=694711)

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170324111806.jpg)

有了攻略的参考，
在二周目一下子跳了七个奖杯：
- 「冥想」
- 「临界点」
- 「海市蜃楼」
- 「冒险」
- 「升华」
- 「远古生物」
- 「登峰造极」

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170324115050.jpg)

从此在三周目变身「白袍」，再也不愁围巾的能量了 lol

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170324122208.jpg)

>二周目遇到的同伴

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170324123029.jpg)

## Chapter 4
为了体验作为白袍的感觉，顺便再解锁「历史」与「试炼」这两个本该在二周目解锁的成就，于是在当天晚上又老老实实地过了一次三周目

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170324224344.jpg)

三周目只身一人，直到通关都没有邂逅其他的旅人，莫名感到有一丝孤独，一直在期待屏幕四周会有白光亮起。
>当出现同伴时，会根据对方所在位置在屏幕边缘亮起一道白光


## Chapter 5
隔了一周没有踏上旅途，是为了「归来」

>在熟悉的启动界面跳杯了

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402111756.jpg)

至此剩余「至交」，「奇迹」和「探索」三个奖杯

「探索」需要在沙漠中找到所有魔带，便直接移动到沙漠，根据网友制作的路线图跳杯了。
![](https://qiniu.viosey.com/img/Journey-Desert.jpg)

随之遇到了一个红袍旅人，本打算带着他直到通关，然而没过多久他消失了，看着他打坐，身体从上到下逐渐消逝... 明知这是无可挽回的事情，我却拼命按着「○」键想改变些什么。

似乎希望没了，继续前行的心情也没了。

## Chapter 6
开始全新的游戏，希望着能遇到一个默契的同伴。

进入了断桥关，正无聊的等待同伴时，突然「奇迹」跳杯了！但是屏幕四边并没有亮起白光呀，疑惑的四处观望，突然发现高处有一个白色的小点，这时有点小激动，感觉对方也是一个白袍。

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402124626.jpg)

我们两人开始朝对方移动，走进了看到他确实是个白袍，而且袍文两人都一样多。

>一起飞越断桥

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402125153.jpg)

>似乎两人的围巾也一样长

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402125401.jpg)

>你看！这里有朵花！

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402125712.jpg)

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402130315.jpg)

>同时跳下悬崖

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402130740.jpg)

当我们跳下悬崖之后，过了一会儿他也开始消失了... 这时心情别提有多难过了，难得遇到一个十分默契的同伴。而且觉得他似乎知道里世界的入口，正盼他带我进入里世界呢

我在这里飞来飞去，不知道在等些什么，也不愿意再独自走下去。

过了不知道多久，屏幕亮起白光，怔了一下，便看向对方，
桥豆麻袋！那个符号！他回来了！

失而复得的激动无法言表

当近乎绝望的时候，他带着那道白光，从天空飘然而下。

>廊桥

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402131321.jpg)

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402131433.jpg)

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402132258.jpg)

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402133310.jpg)

到了雪山的时候，他没有朝着前方行走，似乎是在卡地图的 bug，果然我的猜想是正确的，他知道进入里世界的方法 (๑•̀ㅂ•́)و✧

>bug 位置

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402134334.jpg)

>成功进入了里世界

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402134746.jpg)

>里世界中的神庙

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402135000.jpg)

>通向重生之路

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402135345.jpg)

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402135621.jpg)

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402135852.jpg)

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402135912.jpg)

![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402141309.jpg)

Nice to meet you, ANARKY420
![](https://qiniu.viosey.com/img/%E9%A3%8E%E4%B9%8B%E6%97%85%E4%BA%BA_20170402142148.jpg)