---
title: 树莓派初始化配置：更换源 & 连接 WiFi & 静态 IP
date: 2017-01-20 01:55:08
tags:
- 树莓派
- Raspberry Pi
- 镜像源
categories:
- 技术向
thumbnail: https://cdn.viosey.com/img/blog/raspi-network-black-tiny.png!blogthumbnail
---

去年年底 get 了一个 Pi 3，一个 Pi 2。重装了 n 次系统，各种折腾，然后就整理了一些参考资料。
<!-- more -->
最重要的首先是配置好树莓派的网络环境啦。

## 更换国内镜像源

很多教程没有写完整，更换源需要修改两个文件。
经过我的测试，发现两个文件分别使用阿里和中科大的效果最好。

>发布这篇文章时，树莓派最新版本是 jessie。如有更新请修改版本名

### source.list
使用阿里镜像源

```bash
sudo nano /etc/apt/sources.list
```

将原来的配置注释掉，添加第二行即可

```bash
#deb http://mirrordirector.raspbian.org/raspbian/ jessie main contrib non-free rpi
deb http://mirrors.aliyun.com/raspbian/raspbian/ jessie main contrib non-free rpi
# Uncomment line below then 'apt-get update' to enable 'apt-get source'
#deb-src http://archive.raspbian.org/raspbian/ jessie main contrib non-free rpi
```

### raspi.list
使用中科大镜像源

```bash
sudo nano /etc/apt/sources.list.d/raspi.list
```

将原来的配置注释掉，添加第二行即可

```bash
#deb http://archive.raspberrypi.org/debian/ jessie main ui
deb http://mirrors.ustc.edu.cn/archive.raspberrypi.org/debian/ jessie main ui
# Uncomment line below then 'apt-get update' to enable 'apt-get source'
#deb-src http://archive.raspberrypi.org/debian/ jessie main ui
```

## 连接 WiFi
编辑 wifi 文件
```bash
sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
```

在该文件最后添加：

```bash
network={
  ssid="wifiname"
  psk="password"
}
```

>引号部分分别为 WiFi 的名字和密码

保存文件后几秒钟应该就会自动连接到该 WiFi
或者执行

```bash
sudo ifdown wlan0
sudo ifup wlan0
```

查看是否连接成功

```bash
ifconfig wlan0
```

## 设置静态 IP 地址

```bash
sudo nano /etc/network/interfaces
```

```bash
allow-hotplug wlan0
iface wlan0 inet static # 将 manual 改为 static
	address 192.168.1.111 # 静态地址
	netmask 255.255.255.0 # 网络掩码
	gateway 192.168.1.1 # 网关
	network 192.168.1.1 # 网络地址
	wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
```

`wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf`  可以替换为以下设置方式

```bash
wpa-ssid Your_Wifi_SSID
wpa-psk Your_Wifi_Password
```
