---
title: Enjoy your Feeling —— Poker 3 与 Dvorak 的结合
date: 2016-01-31 15:36
tags: 
- 机械键盘
- 外设
- Poker 3
category:
- 分享境
thumbnail: https://cdn.viosey.com/img/blog/poker3_white.jpg!blogthumbnail
id: 12
---
**60% 键盘**，**高度可定制化**，对小众的 Poker 3 长草了几周后，最终还是仍不住拔了草。
冰蓝浸染的键帽甚是喜欢，有着一股清爽怡人的感觉，不过还是有些遗憾键帽不是无刻的。

<!--more-->

![](https://qiniu.viosey.com/imgpoker3-overlook.jpg)

---
![](https://qiniu.viosey.com/imgpoker3-box.jpg)

Poker 3的箱子非常的朴素，只有几个logo。制造POKER 3的厂商有Vortex，KBC和iKBC（看起来混杂，实际上这都是同一个厂商），KBC应该就是“可编程”的首字母缩写。目前比较靠谱的说法是这个键盘是中国设计的（也有说是台湾设计不过都一个意思）

---
![](https://qiniu.viosey.com/imgpoker3-desktop.jpg)

Poker 3十分的小巧，长度比MacBook还短了一些。窄边框似乎已经成为了趋势，显示器，手机，现在连键盘也开始搞窄边框了。

---
![](https://qiniu.viosey.com/imgpoker3-back.jpg)

因为第三代底板为全金属，导致牺牲了便携性，但白色的金属底板与金属铭牌看起来很养眼。Poker 3的前后的高度差已经足够了，没有配增高架也让整个键盘更简约。

---
![](https://qiniu.viosey.com/imgpoker3-switch.jpg)

背部的四个开关。可以对colemak，qwerty，Dvorak三个键位进行切换，自定义fn,pn键的位置，将capslock切换为fn

---
![](https://qiniu.viosey.com/imgpoker3-detail.jpg)

Poker 3有4层键位，其中3层 (Layer2,Layer3,Layer4)可编程。除了Default层，切换其他三层后空格键会分别亮蓝, 红, 紫光。
Poker 3的键位编程是硬件层面，所以即使换了电脑 自定义的键还是一样的。

---
---
大概高中的时候 就一直想练习Dvorak键位，但因为碎片化的时间一直没练成。而恰好poker 3自带了Dvorak键位，也省了插件改键的繁琐，便开始对Poker 3进行调教，方便Poker 3 与 Dvorak的融合。
Poker 3直接砍掉了43个键位，然后把很多键位的功能变成了组合键集成在了其他键位上（Fn+），比如方向键是Fn+I/J/K/L，Fn键便变得十分重要。
**于是先将背部的开关3打开，把CAPSLOCK切换为Fn，用左手的小拇指按Fn更方便使用组合键。**
**然后把开关1打开，把qwerty换成Dvorak，Dvorak才是真正的目的。**
按照AppleKeyboard的使用习惯左下三个功能键依次是Ctrl，Option，Command，而Poker 3是Ctrl，Command，Option，这个解决方案很简单，**在系统偏好设置改一下就行了（如下图）。**
![](https://qiniu.viosey.com/imgkeyboardModifierKey.png)
最后 因为对Dvorak指法还不是很熟练，以防某些情况需要快速打字，所以我把Layer 4的键位改成了qwerty键位。

---
做完上面的前三个步骤基本奠定了改键方案，但是那三个步骤都有一些漏洞和缺陷，于是祭出改键神器----**[Karabiner](https://pqrs.org/osx/karabiner/)**

第一步因为换掉了CAPSLOCK，于是把**Ctrl+Shift**作为**CAPSLOCK**
第二步换了Dvorak键位，以前的**Command+** 的位置便变得有些别扭，用Karabiner修改了一下，大概如下
>command+; to command+z
command+q to command+x
command+j to command+c
etc...

第三步对换了command和option，结果空格右侧的command也变了，所以把**OPTION_R** 改成 **COMMAND_R**

[相关private.xml](https://github.com/viosey/karabiner)

---
<center> At Last 
用Poker 3和Dvorak写下了这篇文章
</center>