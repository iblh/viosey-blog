---
title: Carbon 主题
date: 2016-03-20 20:12
tags: 
- typecho
- theme
category: 
- 创作集
thumbnail: https://qiniu.viosey.com/imgcarbon1.png
id: 18
---
## Carbon
一款极简的 typecho 主题

A minimalist typecho theme.

<!--more-->

## 简介
- 极简主题
- [Github 地址](https://github.com/viosey/Carbon)
- [Demo 演示](https://lab.viosey.com/carbon)

## 预览
![](https://lab.viosey.com/carbon/usr/themes/Carbon/screenshot.jpg)

![](https://o27z61k07.qnssl.com/imgcarbonarticlepage.png)

## License
Open sourced under the GPL license.