---
title: 或许是 Hexo 最酷的 Material Design 主题
date: 2016-11-12 15:10:45
tags:
- Hexo
- Material Design
- Theme
category:
- 创作集
id: 38
thumbnail: https://cdn.viosey.com/img/blog/material-landscape-tiny.png!blogthumbnail
---
从国庆假期开始就在着手这个主题的开发，花(tiao)费(piao) 了一个多月后，终于在 `双十一`(过后的5个小时) 内发布了 1.0.0 正式版本。

<!-- more -->

## Why?
>这个主题最初是为 typecho 而开发的 [typecho-theme-material](https://github.com/viosey/typecho-theme-material)。
后来觉得 hexo 用起来更 geek，便移植到 hexo。
因为 hexo 开发有极大的自由度，这个主题也变得更加完善。

内置三种主题样式，三种 Markdown 样式（仍在逐渐增加）；
无论是背景，各种组件颜色还是图标都可以轻松自定义；
集成多种独立页面和第三方服务。

从 `极简风` 到 `多图流`，都能完美驾驭。

## More:
### 介绍及文档：
[**Material Theme**](https://material.vss.im/)

### Github: 
[**hexo-theme-material**](https://github.com/viosey/hexo-theme-material)

### Demo: 
[Paradox 样式](https://blog.viosey.com)
>因未能提供全部样式的实时预览，Demo 使用的是 Paradox 样式。

### 快速体验
开始定制独一无二的主题：

```bash
git clone https://github.com/viosey/hexo-theme-material.git
```
或

```bash
npm install hexo-material
```

## 渲染：
![](https://qiniu.viosey.com/img/Material-Phone-Render.png)

![](https://qiniu.viosey.com/img/Materia-overview-tiny.png)