---
title: SemVer 语义化版本
date: 2016-10-17 19:36:59
tags:
- SemVer
- 版本号
- Semantic
category:
- 分享境
thumbnail: https://cdn.viosey.com/img/blog/semver.png!blogthumbnail
id: 34
---
最近开发 hexo 版本的 Material 主题，发布了个 npm 包，就顺便了解了一下 Semantic Version。<!-- more --> 随便命名的习惯得改改了😂。

## 版本格式
`Major.Minor.Patch` 为版本号格式

- Major：表示主版本号。当你做了不兼容的 API 修改，例如软件重构，或出现不向下兼容的改变时增加  `Major`。当 `Major` 为 0 时表示还处于开发阶段。
- Minor：表示次版本号。当你做了向下兼容的功能性更新，出现新功能时增加 `Minor`。
- Patch：表示修订号。如 bug 的修复，只要为向下兼容的修改都是增加 `Patch`。

## 修饰词
- alpha：内部版本。
- beta：测试版。
- rc：即将作为正式版发布，release candidate 的缩写。
- lts：长期维护，long term support 的缩写。

## 规范
1. 版本号必须为非负的整数，且禁止在数字前补零。
2. 标记版本号发现后，禁止改变该版本内容，任何修改都必须以新版本发现。
3. 次版本号 `Minor` 可因修订级别 `Patch`  的改变而递增；主版本号 `Major` 可因次版本号 `Minor` 及修订级别 `Patch` 的改变而递增。
4. 次版本号 `Minor` 递增时，修订号 `Patch` 必须归零。
5. 主版本号 `Major` 递增时，次版本号 `Minor` 和修订号 `Patch` 必须归零。
6. 修饰词与版本号之间用 `-` 连接。范例：`1.0.0-alpha`

## 参考
[Semantic Versioning 2.0.0](http://semver.org/)