---
title: Shape Shifter
date: 2016-07-16 11:37
tags:
- Canvas
- 粒子动效
category: 
- 分享境
thumbnail: https://cdn.viosey.com/img/blog/shapeshifter.jpg!blogthumbnail
id: 26
toc: false
---
在 hacpai 看到了它的 halt 页面, 感觉挺好看的

![](https://cdn.viosey.com/img/blog/shapeshifter.jpg)

这里是个 [DEMO](http://www.kennethcachia.com/shape-shifter/)

然后我把代码放在了 gist 上面 [Shape Shifter](https://gist.github.com/viosey/35111f2152eb04401b5046e4b420d308)

想要修改显示的文字只需要修改代码第55行

```
 S.UI.simulate("I'm|Viosey|Nice|to|Meet YOU!|#countdown 3");
```