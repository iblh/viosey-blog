---
title: hexo 主题开发文章缩略图功能
date: 2016-09-20 23:29
tags:
- hexo
- Development
- thumbnail
category: 
- 技术向
thumbnail: https://cdn.viosey.com/img/blog/tetris_light_cubes.jpg!blogthumbnail
id: 31
toc: false
---
最近打算把该主题移植到 hexo 上面, 首要问题就是文章缩略图, 当初做 typecho 主题的时候也在这里遇到了瓶颈.

<!-- more -->

首先是随机缩略图, 卡了好几天, 后来的解决思路是: 
>先获取每个需要加缩略图的选择器, 然后生成随机数, 给每个选择器加一个 id, 取对应该随机数的图片作为缩略图.
(其实也可以不加 id, 只是为了以后有可能的扩展先加上)

先创建一个 thumbnail.ejs 的组件, 代码如下

```javascript
<script>
	var number_of_banners = 5; //缩略图数量
	var randomNum;
	
	var locatePost = $(".something-else").next(); //获取位置
	
	for(var i=0;i<<%= theme.per_page %>;i++){
		randomNum = Math.floor(Math.random() * number_of_banners + 1);
		
		locatePost.children(".post_thumbnail-random").attr('id', 'random_thumbnail-'+randomNum);
		locatePost.children(".post_thumbnail-random").css('background-image', 'url(' + '<%= theme.img.random_thumbnail %>' + randomNum + '.jpg' + ')')
		
		locatePost = locatePost.next();
	}
</script>
```

然后随机缩略图到这就 OK 了, 接着是自定义缩略图.
这时思路没转变过来, 想着从文字内取第一张图片作为自定义缩略图, 后来想到可以在写文章的时候自定义缩略图链接, 这就简单许多了.

接着就是加个判断是否填入自定义缩略图链接, 如果没有的话则使用随机缩略图.

最后剧透一下, 缩略图功能完成了, hexo 的主题还会远吗