---
title: Hello 2017
date: 2017-01-02 00:44:04
category:
- 事件薄
tags:
- 2017
- diary
thumbnail: https://cdn.viosey.com/img/blog/hello-2017.png!blogthumbnail
id: 39
toc: false
---
有段时间没有写博客了呢，期末周各种忙。
<!-- more -->
一个月内还重装了两次电脑，第一次是因为手滑升级了 10.12，结果发现 karabiner 用不了，不得不重装回 10.11；第二次是上个星期安装了 little snitch，重启后电脑炸了(╯°□°）╯︵┻━┻ 只好在恢复模式把资料拷贝出来之后再重装一次系统。
第一次重装忘记拷贝了 一些密钥，第二次重装发现 Quiver 的一些笔记没有备份！整理好的树莓派的教程也都没了，一直拖着没有发布到博客（拖延症害死人呐！）

回顾 2016 年，感觉收货颇多，搞了 [typecho](https://github.com/viosey/typecho-theme-material) & [hexo](https://github.com/viosey/hexo-theme-material) 的 Material 主题，花了一个多月学了托福，然后还入了一点 laravel 坑。
认识了很多有趣的小伙伴，GitHub 猛涨了 1000 star。

2016 剁手了一把 Poker3 的茶轴键盘，一个 IE80，还有 Raspberry Pi 3B + 2B（2B 收的二手，自带触摸屏 + 网卡 + SD 卡）。还有 Steam 的一堆游戏（比如 GTA V, Kerbal Space Program, ICEY），不过基本属于都喜加一。

---
差不多说完了 2016 年，说一下 2017 年的准备和愿望吧。

在 2017.1.1 我开始正式使用微博：[viosey](http://weibo.com/viosey)。

希望在新的一年学到更多的技术，包括但不限于各种前端框架 & Android 开发，然后用两个树莓派折腾出一些好玩的东西来。和一些小伙伴一起造轮子 (oﾟ▽ﾟ)o

---

以下是我的在 2016.12.31 新注册的 PGP 公钥，密钥由我所有

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBFhnVSgBCADTsLKrDGQp3m+stbKqrrYByNaWru96iS34M2zPXAy+syiXBvcp
Pz+uTQNQVeqtQVAqhK5TuEG9C1uoWx4UybuiB9pOqcD/ClNoKn+aXb9uF0XsTiaq
PGUmMJ12zZUslWE+VzZ7rrbqgHBci0G6pih0Ujq1KPoiYmMrRGzOe+BjgYAJegEB
PgBY4qdAfbK62aRQr+Kb4CPziDGuy7PeoseTDflq3MaySgh5KQJhcW/ZM56pEV/I
Hf+Pq7vlQEB16XBHEoqsrrlv/NjTJoJgddLsh7RF9Murt37tHcrxQAXQi4px9X0/
Ik4XsVTq3LWUawgEV/lTMduS0nc8jTBx0YBvABEBAAG0G3Zpb3NleSA8aW12aW9z
ZXlAZ21haWwuY29tPokBPQQTAQoAJwUCWGdVKAIbAwUJB4YfgAULCQgHAwUVCgkI
CwUWAgMBAAIeAQIXgAAKCRDcDVsXTWHvLUsbB/47kp2P4gfZnR3FjWb8TFJDMq3w
db2KqTrUZ85dlrQjpY4MLgbOAakxg4CID85n9d/A5v8dfBZmxiC2x6Ip3J9ld1iR
hfZaVE6QZ+2fHK4t+RkuVXNXTq3cAQlpw04W1Wqfv9Fipu/ecjDaX8a7MDYZ364e
l0UillmO2Q62Igt9ouU13H4kWz90n5KWo20Q0TM+XwgbU4vPSRIhJZklUf2OZsFV
B9nwPnfBJQ3/hQFlYghQKCx5VuwF0JPV96wptiDww7oD5ge21UACjfJ9xIKR7d01
4rGJqmLXfHg/ZrN4b6c2vqGDFf7Y7RbGQBbfLLWaLJ1Tkssg7Se7W0Hm2dAsuQEN
BFhnVSgBCADS6dbVD+vy0MeANPDKIC903kbWWuZ35KCsqJNxu2iClrnCy/ukPWvq
8Z6w4RNOZ0DW3hRm0IGcfWe2C3uPDmfa2iM5Mvwjr9hKuydVnOY5vi2QjvdSeCEx
u6n/VW7zlF4Uek6zRssLrB+EBsLdsj6I9poCOhbrpdYtFaJ4rvHlODmL0mJo98fi
ji10IgTSjVHRq6yyq8uWwcvqjMWvaZRTzjz32zYq2xiEX7Lfr5dc15cbcb8kP1EI
BONu2bzDs4wAYYZpYahOHmqp9lGMctSU70MGSyr37bGznxuWn4Npq/loMoLgwBPW
HQ86aIOjFmjSQA4RoHMbIfQ+P7m5RlifABEBAAGJASUEGAEKAA8FAlhnVSgCGwwF
CQeGH4AACgkQ3A1bF01h7y0c2ggAhbCCK+Eo7rJyybhiMlKL5uZTlUebppoa/UqA
o4vPZIriY/c8ugD9rvU4vJVbdY8v3dK3gHb+irH+NGIkAG/xwLEQiZukWTwlyk9J
vLDPYgM12jEFNdxG5pq4YzO5In8KdbBlRNCx10BCnT2EDg0p27j1t+mvR6leH9V+
E4n4RH00D1KPILfPDc7cA0JcpsiEruGRdsU8lag/vm6meSnAT1SJ8xzV2QpJHpDM
tFVlYUjm2Ia/yJXCWfGCaKapOPVrIwSpWJZ2Wzbvi594rnQzfryiyn6fQ7Nxcn0B
o0lRYV8pIfQPrZIS0uL1iiPtNKUt6TZmvggiu59hmute9phObg==
=giGa
-----END PGP PUBLIC KEY BLOCK-----
```