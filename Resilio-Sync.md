---
title: 在 Ubuntu 上安装 Resilio Sync (BitTorrent Sync)
date: 2016-08-05 16:44
tags:
- Sync
- Ubuntu
category: 
- 技术向
thumbnail: https://cdn.viosey.com/img/blog/resiliosync.png!blogthumbnail
id: 27
---
上次荔枝家的 Sync Pro 版本做活动减 10 元, 于是就买了下来.
用了段时间感觉确实不错, P2P 的方式无论是对个人的设备间同步/还是分享文件都挺方便 (除了还要安利别人也装一个 Sync)

再者 Sync 支持的平台也非常齐全, 从 Mac 到 Linux, WP 到 Fire OS, Netgear 到 Seagate
于是我就想着在 Ubuntu 也安装个客户端 (虽然不知道要做什么)

# 下载和安装

## 命令行安装
```
sudo add-apt-repository ppa:tuxpoldo/btsync
sudo apt-get update
sudo apt-get install btsync-user //作为正常的桌面端使用
sudo apt-get install btsync //作为 BTSync 服务器使用
```

## 二进制文件安装
在 [BitTorrent Sync 官网](https://getsync.com/platforms/desktop/)下载对应的二进制包，然后解压：
```
cd Downloads
tar xvzf btsync_x64.tar.gz -C /opt
```
# 启动
如果使用 1.2 的方式安装, 需要把对应路径加入  $PATH, 然后在终端输入btsync即可启动

# 配置
完整的 config 可以参考：`btsync --dump-sample-config`

在浏览器内打开：`http://127.0.0.1:8888/gui/` (用户名 = admin, 密码 = 安装过程中设置的密码)
或者编辑 `/etc/btsync/`下的配置文件

# 参考
[How to run BitTorrent Sync?](https://askubuntu.com/questions/284683/how-to-run-bittorrent-sync/296130#296130)