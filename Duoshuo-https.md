---
title: 多说对 https 也太不友善了
date: 2016-05-17 00:30
tags:
- 多说
- HTTPS
- SSL
category: 
- 自言语
thumbnail: https://cdn.viosey.com/img/blog/inset-1.jpg!blogthumbnail
id: 22
toc: false
---
启用了多说之后, 首先得把原生的评论同步到多说, 找了个 typecho 插件, 本以为同步十分简单, 没想到多说的页面不支持 https, 于是插件打不开了... 

通过曲线救国把评论同步好了, 又发现启用了多说的页面的小绿锁不绿了, 才发现多说调用的资源全是 http 的, 还好网上的解决方法也挺多的, 对 embed.js 各种修改

本以为就这样结束了, 表情也禁止了使用, 万万没想到 多说会将字符自动解析成表情 \_(:3」∠)\_ 
找到 `r = s.author;` 后 添加以下代码可以将部分表情链接换成 https
```
s.message = s.message.replace(/src=\"http\:\/\//,'src=\"https://');
```
但是还有一部分表情用的是 http://img.t.sinajs.cn/ 的, 于是又得对这个进行修改.

然而如果对含有表情的评论回复的话, 那引用框中的表情仍然是 http [捂脸]