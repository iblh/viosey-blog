---
title: 用插件将 Chrome 打造成利器
date: 2016-04-09 21:31
tags:
- chrome
- 插件
category: 
- 分享境
thumbnail: https://cdn.viosey.com/img/blog/chromebanner.jpg!blogthumbnail
id: 19
---
### 一个页面 一个世界
Chrome 默认的新标签页既不美观也不实用, 用好 Chrome, 从标签页开始
- [远方 New Tab](https://chrome.google.com/webstore/detail/dream-afar-new-tab/henmfoppjjkcencpbjaigfahdjlgpegn)
每天打开新页面都是一段不期而遇的旅行
- [Earth View from Google Earth](https://chrome.google.com/webstore/detail/earth-view-from-google-ea/bhloflhklmhfpedakmangadcdofhnnoh)
每次打开新页面都能被Google Earth卫星照片所惊艳
- [iChrome](https://chrome.google.com/webstore/detail/ichrome-a-fast-productive/oghkljobbhapacbahlneolfclkniiami)
可以任意定制的主题和小部件, 数以千计的背景图片和丰富的功能
- [Infinity](https://chrome.google.com/webstore/detail/infinity-new-tab/dbfmnekepjoapopniengjbcpnbljalfg)
客制化你的上网导航，200多个扁平化网站图标, 每日一张高清壁纸, 同样具有便捷的小工具


### 广告终结者
带来清爽的上网体验 你们资瓷不资瓷呀?
- [Adblock Plus](https://chrome.google.com/webstore/detail/adblock-plus/cfhdojbkjhnklbpkdaibdccddilifddb)
超千万用户的免费开源项目
- [uBlock](https://chrome.google.com/webstore/detail/ublock/epcnnfbjfcgphgdmggkamkmgojdagdnn?hl=zh-CN)
一款高效的请求过滤工具, 占用极低的内存和CPU

### 沉浸阅读
没有了广告确实清爽了许多, 但是如果想要静静地阅读一篇文章, 而页面又有一些干扰元素存在, 那就需要这些插件来拯救你的阅读体验
- [阅读模式](https://chrome.google.com/webstore/detail/reader-view/iibolhpkjjmoepndefdmdlmbpfhlgjpl)
比Safari阅读模式功能更完善的轻量级插件, 可设置背景色, 字体以及字体大小
- [EasyReader](https://chrome.google.com/webstore/detail/easyreader/boamfheepdiallipiieadpmnklbhadhc)
功能更强大的阅读插件, 可自选区域, 自动识别目录, 还能对样式进行详细的设置


### 保存媒体文件有妙招
遇到漂亮的图片还在一张张保存? 遇到好听的音乐还在找下载按钮?
有了这些插件, 一键保存到本地, 随时欣赏 (๑•̀ㅂ•́)و✧
- [Fatkun 图片批量下载](https://chrome.google.com/webstore/detail/fatkun-batch-download-ima/nnjjahlikiabnchcpehcpkdeckfgnohf)
找出当前页面的所有图片，提供按分辨率、链接等筛选图片
- [Image Downloader](https://chrome.google.com/webstore/detail/image-downloader/cnpniohnfphhjihaiiggeabnkjhpaldj)
浏览并下载当且网页上的图片
- [声海盗](https://chrome.google.com/webstore/detail/%E5%A3%B0%E6%B5%B7%E7%9B%97/idleenniidjlnmnjkjmmnocnkmjibadd)
一键下载当且网页所播放的音乐


### Coder 专区
既然搞定了基本需求, 那就开始干活吧!
怎么少得了这些 Web 前端开发神器
- [Chrome Sniffer Plus](https://chrome.google.com/webstore/detail/chrome-sniffer-plus/fhhdlnnepfjhlhilgmeepgkhjmhhhjkh)
探测当前网页正在使用的开源软件或者js类库
- [Page Ruler](https://chrome.google.com/webstore/detail/page-ruler/jlpkojjdgbllmedoapgfodplfhcbnbpn)
用鼠标画出尺子, 获得具体像素和位置, 并可以测量任何网页上的元素
- [Web Developer Checklist](https://chrome.google.com/webstore/detail/web-developer-checklist/iahamcpedabephpcgkeikbclmaljebjp)
分析网站不足之处
- [YSlow](https://chrome.google.com/webstore/detail/yslow/ninejjcohidippngpapiilnmkgllmakh)
根据雅虎军规的页面性能分析工具让你的网页更快
- [Window Resizer](https://chrome.google.com/webstore/detail/window-resizer/kkelicaakdanhinjdeammmilcgefonfh)
调整浏览器窗口来模拟各种屏幕分辨率
- [WhatFont](https://chrome.google.com/webstore/detail/whatfont/jabopobgcpjmedljpbcaablpmlmfcogm)
识别网页用了哪些字体, 以及字体的大小
- [ColorPick Eyedropper](https://chrome.google.com/webstore/detail/colorpick-eyedropper/ohcpnigalekghcmgcdcenkpelffpdolg)
从网页上取色, 获取颜色代码/RGB

 
### 如何优雅地使用 Chrome?
进一步提高 Chrome 使用效率和舒适度, 使用 Chrome 将更加如鱼得水
- [为什么你们就是不能加个空格呢](https://chrome.google.com/webstore/detail/%E7%82%BA%E4%BB%80%E9%BA%BC%E4%BD%A0%E5%80%91%E5%B0%B1%E6%98%AF%E4%B8%8D%E8%83%BD%E5%8A%A0%E5%80%8B%E7%A9%BA%E6%A0%BC%E5%91%A2%EF%BC%9F/paphcfdffjnbcgkokihcdjliihicmbpd)
自动在网页中所有的中文字和半形的英文、数字、符号之间插入空白
- [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
浏览器用户脚本管理器
- [Proxy SwitchyOmega](https://chrome.google.com/webstore/detail/proxy-switchyomega/padekgcemlokbadohgkifijomclgjgif)
轻松快捷地管理和切换多个代理设置
- [Vimium](https://chrome.google.com/webstore/detail/vimium/dbepggeogbaibhgnhhndojpepiihcmeb)
键盘流/Vim党必备


### 管理好你的臃肿插件
装了以上辣么多的插件, Chrome 工具栏和电脑内存都要炸了. 以下两个插件, 即可自定义情景模式也可逐个开关插件
- [Context](https://chrome.google.com/webstore/detail/context/aalnjolghjkkogicompabhhbbkljnlka)
分组排序扩展并轻易切换, 不同环境一键切换插件
- [一键管理所有扩展](https://chrome.google.com/webstore/detail/%E4%B8%80%E9%94%AE%E7%AE%A1%E7%90%86%E6%89%80%E6%9C%89%E6%89%A9%E5%B1%95/niemebbfnfbjfojajlmnbiikmcpjkkja)
一键开启/关闭扩展,扩展的逐个禁用和开启