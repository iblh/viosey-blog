---
title: Surge 基本配置指南
date: 2016-02-21 21:14
tags: 
- surge
- APP
- iOS
category: 
- 分享境
thumbnail: https://cdn.viosey.com/img/blog/surge-logo.png!blogthumbnail
id: 15
---
### What is Surge?

Surge 是基于 iOS 9 的新特性 Network Extension 开发的一款网络抓包调试工具。

<!--more-->

Surge的工作原理是使用 Packet Tunnel Provider 给系统套上一个代理，Surge 有两个主要组件：Surge 代理服务器和 Surge TUN 接口。程序运行之后，Surge 能够捕获监听 iOS 设备上所有的 HTTP / HTTPS / TCP 请求流量，甚至还能将流量按照自定义的规则分流重定向到指定的 HTTP / HTTPS / SOCKS5 代理服务器来处理所有的 HTTP/HTTPS 流量。针对一些不服从系统代理设置（如 Mail.app ）的应用程序 ，将由 Surge 的 TUN 接口来进行处理。


### What can Surge do？
iOS本身是不支持SSL/socks5/shadowsocks方式的，利用 Surge 可以在不越狱 iPhone / iPad 的情况下实现很多功能，比如观察各种 APP 的联网情况、监视 HTTP / TCP 请求、网络调试、流量跟踪、流量统计、广告拦截、屏蔽指定网址、以及实现那个非常重要的功能等等。


### How to use Surge?

#### 代理设置
在代理设置界面，可以添加三种类型的代理，http|https|socks5
ss代理在界面不可以直接添加，只能通过直接编辑配置文件，添加custom类型的代理。


#### 预定义类型的规则
DOMAIN: 域名完全匹配
DOMAIN-SUFFIX: 匹配域名的结尾部分
DOMAIN-KEYWORD: 域名中包含这个关键字
IP: 如果请求的ip地址在范围中， 使用ip加mask的形式，例如：192.168.1.1/24
GEOIP: 基于ip的地理位置，例如CN(中国)，US(美国)等等

#### 满足规则时的处理方式

DIRECT: 也就是我们的直连，必经过任何转发，用于访问国内网络
REJECT: 拒接链接，比较多的是拦截特定的包，例如广告请求
[代理名称]: 默认是没有这一项的，当我们添加了不同代理之后，没一个添加的代理也会出现在这里，所以，我们可以将特定的网络请求转发到一个我们预定义的代理上面，这也是surge中本文最关心的功能。

#### 规则列表中的最后两项
规则列表中只有必要的一些域名或 IP，大多数时候 Recent Requests 记录的 Policy 部分，能看到的是规则配置文件最后两项判断：
DIRECT（GEOIP CN → DIRECT），域名判断结果如果是中国走直连；
Proxy（FINAL → Proxy），兜底规则，前面规则判断完还没明确的基于这条规则走代理。
从这里也能看出规则表的自定义中，至少要保留最后一条「FINAL,Proxy」规则体系才是完整的，而倒数第二条国别判断可以根据你所在的国家进行修改。

#### 多个服务器配置（服务器覆盖文件）
主文件（如，Main.conf）是个独立的标准文件，所以其中 Proxy 部分是必须的，服务器覆盖配置文件（如，HK.conf、TYO.conf）中只写一个服务器地址，并且这个地址不需要和 Main.conf 的中 Proxy 部分形成对应关系。
```
#!PROXY-OVERRIDE:Main.conf
```
#### 配置规则样例
```
# 规则设置
[Rule] 
# 基于域名判断并屏蔽（REJECT）请求
DOMAIN,pingma.qq.com,REJECT

# 基于域名后缀判断屏蔽（REJECT）请求
DOMAIN-SUFFIX,flurry.com,REJECT

# 基于关键词后缀判断走代理（Proxy），强制不尊重系统代理的请求走 Packet-Tunnel-Provider
DOMAIN-KEYWORD,google,Proxy,force-remote-dns

# 基于域名后缀判断请求走直连（DIRECT）
DOMAIN-SUFFIX,126.net,DIRECT

# Telegram.app 指定“no-resolve”Surge 忽略这个规则与域的请求。 
IP-CIDR,91.108.56.0/22,Proxy,no-resolve 
# 判断是否是局域网，如果是，走直连
IP-CIDR,192.168.0.0/16,DIRECT
# 判断服务器所在地，如果是国内，走直连
GEOIP,CN,DIRECT

# 其他的走代理
FINAL,Proxy
```

#### SS配置介绍
```
[Proxy]
# http, https, socks5, custom
Proxy = custom, ip, port, Methor, password, module_url
```
ip, port, Methor, password, module\_url 分别代表了你的shadowsoacks的服务器IP，端口，加密方式，密码。module_url为一个外部模块，相当于ss的规则配置。

### 参考文档
>[Surge 作者：App Store 购买前说明](https://medium.com/@Blankwonder/surge-appstore-%E8%B4%AD%E4%B9%B0%E5%89%8D%E8%AF%B4%E6%98%8E-4bf1feb58c44)
[Surge 新手使用指南](https://medium.com/@scomper/surge-%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6-a1533c10e80b)
[Surge 定制自己的规则配置](https://medium.com/@scomper/surge-%E5%AE%9A%E5%88%B6%E8%87%AA%E5%B7%B1%E7%9A%84%E8%A7%84%E5%88%99%E9%85%8D%E7%BD%AE-34a6d74b0434#.dvxhj6oyx)
[Surge 官方手册 (英文)](http://surge.run/manual/)
[Surge 原理与实现](https://medium.com/@Blankwonder/surge-原理与实现-8aa3304fb3bb)
@jason5ng32 分享的：[Github](https://gist.github.com/jason5ng32/648597df6ca7da5aeb41)
@janlay 分享的：[Github](https://gist.github.com/janlay/b57476c72a93b7e622a6)
@shanskc 分享的：[Github](https://gist.github.com/kaisc/66612d058f6c6b09a3ae)
@imsoff 分享的：[Github](https://gist.github.com/soffchen/47a44825626b1cbd0948)
[Surge.conf for Geeks](http://proxy.sofi.sh/)