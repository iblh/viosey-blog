---
title: 用 illustrator blend tool (AI 混合工具) 制作文字特效
date: 2016-09-10 14:37
tags:
- AI
- illustrator
- Design
category: 
- 分享境
thumbnail: https://cdn.viosey.com/img/blog/viosey_blend-header.png!blogthumbnail
id: 30
---
刷知乎的时候发现 AI 的混合工具挺好玩的, 一不小心就玩了整整一个上午 -.-

<!-- more -->

------

效果图:
![viosey blend 透明背景](https://cdn.viosey.com/img/blog/viosey_blend-trp.png)

## Step 1

输入文字
![AI blend step1](https://cdn.viosey.com/img/blog/AI_Blend-1.png)

## Step 2

复制一层文字, 位置与另一层相同.
>e.g. 一层为「文字」, 一层为「残影」
可将「文字层」隐藏 方便操作

然后选择「残影层」, 右键「创建轮廓」
![AI blend step2](https://cdn.viosey.com/img/blog/AI_Blend-2.png)

## Step 3

填充为空, 边缘可自行选择粗细

>粗细是个很重要的因素, 粗了不灵动, 细了太飘忽

![AI blend step3](https://cdn.viosey.com/img/blog/AI_Blend-3.png)

## Step 4

选择「混合工具」 (快捷键: w )
按顺序点击每个文字

刚开始可能特别丑, 不过点完全部文字之后, 回车(或者双击混合工具图标)
可以选择「间距」然后调整不同的数值直至满意为止

![AI blend step4](https://cdn.viosey.com/img/blog/AI_Blend-4.png)

## Step 5

以自己喜好的顺序点完全部文字(如穿针引线般)
调整好间距

![AI blend step5-1](https://cdn.viosey.com/img/blog/AI_Blend-5.png)

>文字的颜色也需要不断调整到较好的效果

最后把「文字层」显示出来就 OK 了
![AI blend step5-2](https://cdn.viosey.com/img/blog/AI_Blend-6.png)

## Colors 

![](https://cdn.viosey.com/img/blog/viosey_blend-clr.png)
