---
title: 弃用多说，拥抱 Disqus
date: 2017-01-11 20:23:29
tags:
- Disqus
- 多说
category:
- 事件薄
thumbnail: https://cdn.viosey.com/img/blog/disqus-header.png!blogthumbnail
---
自从多说服务开始不稳定的那天起，迁移 Disqus 的念头就在我心中埋下了。
<!-- more -->
本想着在放寒假时再进行迁移，但突然发现今天的日期很有意思 1.11，刚好可以记录在 milestone 上，于是就提前抛弃多说这个毒瘤。不过对于之前评论的小伙伴表示遗憾，你们的头像都没了...

## 为何弃用多说？
多说近期频繁的崩坏实在是受不了。虽然解决了多说 https 的问题，但这也算是多说的一个黑点。除此之外，各种垃圾信息刷评论也是很烦。

即使多说有丰富的社交登录和极大的用户基数；即使 Disqus 被墙，但还是决定告别多说。
有了条件的限制，Disqus 的评论质量也许会更高一些，欢迎自带梯子的小伙伴评论~

## 如何迁移并使用 Disqus
迁移到 Disqus 需要感谢 [duoshuo-migrator](https://github.com/JamesPan/duoshuo-migrator) 这个轮子，极大简化了迁移工作。

迁移步骤在插件中也有很详细的描述。

在迁移中遇到了一个小坑：
发现有一些评论没有迁移过来，是因为生成的迁移 XML 文件中文章链接有一些错误，修改之后就 OK 了。
