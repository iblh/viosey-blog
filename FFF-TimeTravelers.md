---
title: 穿越时空去撩妹
date: 2016-04-28 01:07
tags: 
- Movie
category: 
- 分享境
thumbnail: https://cdn.viosey.com/img/blog/interstellar-spaceship.jpg!blogthumbnail
id: 21
---
五一就快到了, 如果宅着那怎么能没有电影相伴呢, 一起来看看穿越时空的男主们都干了些什么吧 (,,• ₃ •,,)

<!--more-->


>如果能够穿越时空, 你想要做些什么? 
这些男主用实际行动告诉你, 他们既不用来改变世界也不用来发家致富, 他们只用来———撩妹! 

##### 时空恋旅人 About Time
美好的穿越, 美妙的旅程. 教你如何正确的穿越时空去撩妹
[豆瓣影评](https://movie.douban.com/subject/10577869/) [在线观看](http://www.bilibili.com/video/av3205003/) [豆瓣 8.6]

##### 源代码 Source Code
这个故事告诉了我们 "一回生, 二回熟", 日久就生情了
[豆瓣影评](https://movie.douban.com/subject/3075287/) [在线观看](http://www.acfun.tv/v/ac2083656) [豆瓣 8.3]

##### 时间旅行者的妻子 The Time Traveler's Wife
或许这是个最差劲的穿越能力, 但毫不妨碍男主开启萝莉养成计划
[豆瓣影评](https://movie.douban.com/subject/1885124/) [在线观看](http://www.bilibili.com/video/av2423475/) [豆瓣 7.7]

##### 触不到的恋人 The Lake House
这部电影的男主干脆就不会穿越了啊喂, 不过人家跨越时空也能把妹给撩到
[豆瓣影评](https://movie.douban.com/subject/1474180/)	[在线观看](http://www.acfun.tv/v/ac2625161) [豆瓣 7.7]

##### 穿越时空爱上你 Kate & Leopold
这部我倒是没有看过, 就不说什么咯
[豆瓣影评](https://movie.douban.com/subject/1304485/)	[在线观看](http://www.acfun.tv/v/ac268646) [豆瓣 7.4]

##### 年鉴计划 Project Almanac
虽然豆瓣评分不高, 不过个人感觉挺有意思哒
[豆瓣影评](https://movie.douban.com/subject/23767433/) [在线观看](http://www.acfun.tv/v/ac1900055) [豆瓣 6.6]

------------


>最后这一部, 穿越时空的并不是他们的身体, 而是他们的五感

##### 你眼中的世界 In Your Eyes
男主大叫 "我知道了, 你来自未来!" 女主冷静回答:"不, 那是时差"
[豆瓣影评](https://movie.douban.com/subject/10549480/) [在线观看](http://www.bilibili.com/video/av2298919/) [豆瓣 7.6]