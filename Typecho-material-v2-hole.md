---
title: Typecho-Theme-Material - Version 2 开坑计划
date: 2016-04-19 16:59
tags:
- typecho
- theme
- Material Design
category: 
- 事件薄
thumbnail: https://cdn.viosey.com/img/blog/materialv2-2.jpg!blogthumbnail
id: 20
---
Typecho-Theme-Material 的 1.0.0 正式版已经发布快 2 个月了, 是时候开始填 2.0.0 的坑啦(๑•̀ㅂ•́)و✧


<!--more-->


欢迎大家多提一些改进建议 (可以在本篇文章下面回复, 也可以在 <a href="https://github.com/viosey/typecho-theme-material/issues" target="_blank">Github</a> 提 issue). 

所有的改进建议都会在下面列出, 争取在 2.0.0 版本实现, 部分无法在 2.0.0 版本实现的, 也会在 Version 2 的后续更新中实现.

- [x] 可选择使用多说评论的功能
- [x] 实现文章内点赞 (需插件支持)
- [x] 增加独立归档页面
- [x] 优化 "设置外观" 页面
- [x] 评论样式改进 (由 @MeowCold 提出)
- [x] 如果缩略图比较白，标题就看不见了 (由 @苏苏 提出)
- [x] 字体显示改进 (由 @Srioe 提出)
- [x] 评论栏 "网站" 输入框失去焦点后与 "http//" 重叠 (由 @KeJun 提出)
- [x] 弹出层无法自动收回 (由 @KeJun 提出)
- [x] 固定 "返回顶部" 按钮 (由 @Hunter 提出)
- [ ] 优化 / 整理代码 (由 @yumemor 提出)
- [x] 移动端由于自定义滚动条样式导致左右两侧出现距离偏差 (由 @李军 提出)